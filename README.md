stages:
  - build-and-test
  - deploy-to-dev
  - deploy-to-prod

build-and-test:
  stage: build-and-test
  image: node
  artifacts:
    paths:
      - ./build

  script:
    - yarn install
    - yarn test
    - yarn build

deploy to dev:
  stage: deploy-to-dev
  only:
    - develop
  image: node
  script:
    - yarn global add surge
    - surge ./build ekn-sharing-gitlab-dev.surge.sh

deploy to production:
  stage: deploy-to-prod
  image: node
  when: manual
  only:
    - main
  script:
    - yarn global add surge
    - surge ./build ekn-sharing-gitlab.surge.sh
